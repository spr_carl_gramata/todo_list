import Vuex from 'vuex';
import Vue from 'vue';
import todos from './modules/todos';
import createdPersistedState from 'vuex-persistedstate'


//load Vuex
Vue.use(Vuex);

//create store
export default new Vuex.Store({
    plugins: [createdPersistedState],
    modules: {
        todos
    }
});